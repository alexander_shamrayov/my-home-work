import React, { useState } from 'react';
import DeleteModal from '../DeleteModal/index';
import imgDelete from '../../assets/img/delete.svg';
import './style.sass';

const Table = props => {
  const [deleteMod, setDeleteMod] = useState(false);
  const [idDel, setIdDelete] = useState('');

  const onDeleteRow = id => {
    props.deleteRow(id);
    setDeleteMod(false);
  };

  const handleDelete = id => {
    setDeleteMod(true);
    setIdDelete(id);
  };

  return (
    <div className="employees-block">
      {props.employeesList ? (
        <table className="table">
          <tbody>
            <tr className="table-row">
              <th>ИФО</th>
              <th>Позиция</th>
              <th>Направление</th>
              <th>Действие</th>
            </tr>
            {props.employeesList.length > 0 ? (
              props.employeesList.map((item, ind) => (
                <tr className="appoint-row" key={`emp${item._id}`}>
                  <td className="appoint-column">{item.fullName}</td>
                  <td className="appoint-column">{item.position}</td>
                  <td className="appoint-column">{item.direction}</td>
                  <td className="appoint-column">
                    <div className="tableBtns">
                      <img
                        src={imgDelete}
                        className="tableImg"
                        alt="delete"
                        onClick={() => handleDelete(item._id)}
                      />
                    </div>
                  </td>
                </tr>
              ))
            ) : (
              <div className="table__none-employees">
                Караул!!! У нас нет сотрудников по такому фильтру!!!
              </div>
            )}
          </tbody>
        </table>
      ) : (
        <div className="table__none-employees">Сотрудники отсутствуют, всё пропало!!!</div>
      )}

      {deleteMod ? (
        <DeleteModal
          idDel={idDel}
          deleteEl={onDeleteRow}
          setDeleteMod={setDeleteMod}
          deleteMod={deleteMod}
        />
      ) : (
        <div />
      )}
    </div>
  );
};

export default Table;
