import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import AddedForm from '../AddedForm/index';
import Filter from '../Filter/index';
import Table from '../Table/index';
import { data } from '../../data';
import './style.sass';

const Employees = () => {
  const [employeesList, setEmployeesList] = useState(data);
  const [filteredEmployeesList, setFilteredEmployeesList] = useState(data);
  const history = useHistory();

  const filterEmployees = (interFullName, choosePosition, chooseDir) => {
    setEmployeesList(
      filteredEmployeesList.filter(item => {
        return (
          (item.fullName.substr(0, interFullName.length) === interFullName ||
            interFullName === '') &&
          (item.position === choosePosition || choosePosition === '') &&
          (item.direction === chooseDir || chooseDir === '')
        );
      })
    );
  };

  const addNewAppointment = (fullName, position, direction) => {
    try {
      data.push({
        _id: data.length + 1,
        fullName: fullName,
        position: position,
        direction: direction,
      });
      history.push('/employees');
    } catch (e) {
      alert('error: ' + e);
    }
  };

  const deleteRow = id => {
    setEmployeesList(
      employeesList.filter(item => {
        return item._id !== id;
      })
    );
  };

  return (
    <div className="employees-block">
      <AddedForm addNewAppointment={addNewAppointment} />
      <Table employeesList={employeesList} deleteRow={deleteRow} />
      <Filter filterEmployees={filterEmployees} />
    </div>
  );
};

export default Employees;
