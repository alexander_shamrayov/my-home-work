import React, { useState, useLayoutEffect, useEffect } from 'react';
import MuiAlert from '@material-ui/lab/Alert';
import { Container, Typography, TextField, MenuItem, Button, Snackbar } from '@material-ui/core';
import { POSITION_FILTER, DIRECTION_FILTER } from '../../constants';
import { useWindowSize } from '../../hooks/useWindowSize';
import './style.sass';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const AddedForm = props => {
  const [fullName, setFullName] = useState('');
  const [position, setPosition] = useState('');
  const [direction, setDirection] = useState('');
  const [width, height] = useWindowSize();
  const [info, setInfo] = useState();
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState('');
  const [severity, setSeverity] = useState('error');

  useEffect(() => {
    if (width < 1025) {
      setInfo(true);
    } else {
      setInfo(false);
    }
  }, [width]);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  const onClickTable = e => {
    if (fullName === '' || position === '' || direction === '') {
      setSeverity('error');
      setMessage('Заполните все поля!');
      setOpen(true);
    } else {
      props.addNewAppointment(fullName, position, direction);
      setSeverity('success');
      setMessage('Успешно добавлено!');
      setOpen(true);
      setFullName('');
      setPosition('');
      setDirection('');
    }
  };

  const addInfo = () => {
    setInfo(false);
  };

  const hideInfo = () => {
    setInfo(true);
  };

  return (
    <div className="Information-shadow">
      <Container fixed>
        {info ? (
          <div className="Information_btn">
            <Button variant="contained" onClick={addInfo} id="addInfoBlockBtn">
              Добавить данные в таблицу
            </Button>
          </div>
        ) : (
          <div className="Information">
            <div className="Information_block">
              <Typography className="Information-Text">ИФО:</Typography>
              <TextField
                type="text"
                id="nameInput"
                value={fullName}
                variant="outlined"
                onChange={e => setFullName(e.target.value)}
              />
            </div>
            <div className="Information_block">
              <Typography className="Information-Text">Позиция:</Typography>
              <TextField
                type="text"
                select
                id="positionInput"
                value={position}
                variant="outlined"
                onChange={e => setPosition(e.target.value)}
              >
                {POSITION_FILTER.map((item, index) => (
                  <MenuItem key={`sorting-item-${index}`} value={item.key}>
                    {item.value}
                  </MenuItem>
                ))}
              </TextField>
            </div>

            <div className="Information_block">
              <Typography className="Information-Text">Направление:</Typography>
              <TextField
                type="text"
                select
                id="directionInput"
                value={direction}
                variant="outlined"
                onChange={e => setDirection(e.target.value)}
              >
                {DIRECTION_FILTER.map((item, index) => (
                  <MenuItem key={`sorting-item-${index}`} value={item.key}>
                    {item.value}
                  </MenuItem>
                ))}
              </TextField>
            </div>

            <div className="Information_btns">
              <Button
                id="addBtn"
                variant="contained"
                onClick={onClickTable}
                disabled={!fullName || !position || !direction}
              >
                Добавить
              </Button>

              <Button id="hideBtn" variant="contained" onClick={hideInfo}>
                Скрыть
              </Button>
            </div>
          </div>
        )}
      </Container>
      <Snackbar open={open} autoHideDuration={4000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity}>
          {message}
        </Alert>
      </Snackbar>
    </div>
  );
};

export default AddedForm;
