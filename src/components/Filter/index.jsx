import React, { useState } from 'react';
import { Container, Typography, TextField, MenuItem, Button } from '@material-ui/core';
import { POSITION_FILTER, DIRECTION_FILTER } from '../../constants';
import './style.sass';

const Filter = ({ filterEmployees }) => {
  const [interFullName, setInterFullName] = useState('');
  const [choosePosition, setChoosePosition] = useState('');
  const [chooseDir, setChooseDir] = useState('');

  const filterFIO = e => {
    setInterFullName(e.target.value);
    filterEmployees(e.target.value, choosePosition, chooseDir);
  };

  const filterPosition = e => {
    setChoosePosition(e.target.value);
    filterEmployees(interFullName, e.target.value, chooseDir);
  };

  const filterDirection = e => {
    setChooseDir(e.target.value);
    filterEmployees(interFullName, choosePosition, e.target.value);
  };

  const handleCancel = () => {
    setInterFullName('');
    setChooseDir('');
    setChoosePosition('');
    filterEmployees('', '', '');
  };

  return (
    <Container fixed>
      <div className="Sort">
        <div className="Sort_block">
          <Typography className="Sort-text">Фильтровать по ФИО:</Typography>
          <TextField
            type="text"
            id="sortInput"
            value={interFullName}
            onChange={filterFIO}
            variant="outlined"
          ></TextField>
        </div>

        <div className="Sort_block">
          <Typography className="Sort-text">Фильтровать по позиции:</Typography>
          <TextField
            id="sortInput"
            select
            value={choosePosition}
            onChange={filterPosition}
            variant="outlined"
          >
            {POSITION_FILTER.map((item, index) => (
              <MenuItem key={`sorting-item-${index}`} value={item.key}>
                {item.value}
              </MenuItem>
            ))}
          </TextField>
        </div>

        <div className="Sort_block">
          <Typography className="Sort-text">Фильтровать по направлению:</Typography>
          <TextField
            id="sortInput"
            select
            value={chooseDir}
            onChange={filterDirection}
            variant="outlined"
          >
            {DIRECTION_FILTER.map((item, index) => (
              <MenuItem key={`sorting-item-${index}`} value={item.key}>
                {item.value}
              </MenuItem>
            ))}
          </TextField>
        </div>
      </div>
      <div className="filter-cancel__block">
        <Button id="addBtn" variant="contained" onClick={handleCancel}>
          Отмена фильтрации
        </Button>
      </div>
    </Container>
  );
};

export default Filter;
