import { Switch, Route, Redirect } from 'react-router-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import Trans from './components/Trans/index';
import Employees from './components/Employees/index';
import './assets/styles/styles.sass';

const App = () => {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/trans" component={Trans} />
          <Route path="/employees" component={Employees} />
          <Redirect from="/" to="/trans" />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
