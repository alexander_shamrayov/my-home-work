export const POSITION_FILTER = [
  { key: 'junior', value: 'junior' },
  { key: 'middle', value: 'middle' },
  { key: 'senior', value: 'senior' },
];

export const DIRECTION_FILTER = [
  { key: 'front', value: 'front' },
  { key: 'back', value: 'back' },
];
